require("dotenv").config();
const util = require("util");
const uuid = require("uuid");
const mysql = require("mysql2-promise")();

const init = async () => {
  mysql.configure({
    host: process.env.RDS_HOSTNAME,
    user: process.env.RDS_USERNAME,
    password: process.env.RDS_PASSWORD,
    port: process.env.RDS_PORT,
    database: process.env.RDS_DATABASE,
  });
  //  try {
  //    await connect();
  //    console.log("Database connected!");
  //  } catch (e) {
  //    console.error(e);
  //  }
};

const queryProductById = async (productId) => {
  return (await mysql.query(`SELECT *
                              FROM products
                              WHERE id = "${productId}";`))[0][0];
};

const queryRandomProduct = async () => {
  return (await mysql.query(`SELECT *
                              FROM products
                              ORDER BY RAND()
                              LIMIT 1;`))[0][0];
};

const queryAllProducts = async () => {
  return (await mysql.query(`SELECT *
                              FROM products;`))[0];
};

const queryAllCategories = async () => {
  return (await mysql.query("SELECT * FROM categories;"))[0];
};

const queryAllOrders = async () => {
  return (await mysql.query("SELECT * FROM orders;"))[0];
};

const queryOrdersByUser = async (userId) => {
  return (
    await mysql.query(`SELECT *
                           FROM orders
                                    INNER JOIN order_items ON orders.id = order_items.order_id
                           WHERE user_id = "${userId}";`)
  )[0]; // Not a perfect analog for NoSQL, since SQL cannot return a list.
};

const queryOrderById = async (id) => {
  return (
    await mysql.query(`SELECT *
                           FROM orders
                           WHERE id = "${id}";`)
  )[0][0];
};

const queryUserById = async (id) => {
  return (
    await mysql.query(`SELECT *
                           FROM users
                           WHERE id = "${id}";`)
  )[0][0];
};

const queryAllUsers = async () => {
  return (await mysql.query("SELECT * FROM users;"))[0];
};

const insertOrder = async (order) => {
  var order_id = uuid.v4();
  await mysql.query(`INSERT INTO orders
                                      VALUES ("${order_id}", "${order.user_id}", ${order.total_amount});`);
  for( const item of order.products ){
    await mysql.query(`INSERT INTO order_items
                       VALUES ("${uuid.v4()}", "${order_id}", "${item.product_id}", ${item.quantity});`);
  }
  return await queryOrderById(order_id);
};

const updateUser = async (id, updates) => {
  const email_str = updates.email ? `"${updates.email}"` : "email";
  const name_str = updates.name ? `"${updates.name}"` : "name";
  const pass_str = updates.password ? `"${updates.password}"` : "password";
  await mysql.query(`UPDATE users
                     SET email = ${email_str}, name = ${name_str}, password = ${pass_str}
                     WHERE id = "${id}";`);
  return await queryUserById(id);
};

module.exports = {
  init,
  queryRandomProduct,
  queryProductById,
  queryAllProducts,
  queryAllCategories,
  queryAllOrders,
  queryOrderById,
  queryOrdersByUser,
  queryUserById,
  queryAllUsers,
  insertOrder,
  updateUser,
};
