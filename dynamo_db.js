require("dotenv").config();
const { DynamoDBClient } = require("@aws-sdk/client-dynamodb");
const {
  GetCommand,
  ScanCommand,
  PutCommand,
  UpdateCommand,
  DynamoDBDocumentClient,
} = require("@aws-sdk/lib-dynamodb");
const uuid = require("uuid");

let client;
let docClient;

const init = () => {
  client = new DynamoDBClient({ region: process.env.AWS_REGION });
  docClient = DynamoDBDocumentClient.from(client);
  console.log("DynamoDB connected!");
};

const queryRandomProduct = async () => {
  const command = new ScanCommand({
    TableName: "Products",
    ExclusiveStartKey: {
      id: uuid.v4()
    },
    Limit: 1,
  });

  const response = await docClient.send(command);
  return response.Items[0];
};

const queryProductById = async (productId) => {
  const command = new GetCommand({
    TableName: "Products",
    Key: {
      id: productId,
    },
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryAllProducts = async (category = "") => {
  const command = new ScanCommand({
    TableName: "Products",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryAllCategories = async () => {
  const command = new ScanCommand({
    TableName: "Categories",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryAllOrders = async () => {
  const command = new ScanCommand({
    TableName: "Orders",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryOrdersByUser = async (userId) => {
  const command = new ScanCommand({
    TableName: "Orders",
    FilterExpression: "user_id = :user_id",
    ExpressionAttributeValues: {
      ":user_id": userId,
    },
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryOrderById = async (userId) => {
  const command = new GetCommand({
    TableName: "Orders",
    Key: {
      id: userId,
    },
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryUserById = async (userId) => {
  const command = new GetCommand({
    TableName: "Users",
    Key: {
      id: userId,
    },
    ProjectionExpression: "id, email",
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryAllUsers = async () => {
  const command = new ScanCommand({
    TableName: "Users",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const insertOrder = async (order) => {
  const order_id = uuid.v4();
  const command = new PutCommand({
    TableName: "Orders",
    Item: {
      "id": order_id,
      "user_id": order.user_id,
      "products": order.products,
      "total_amount": order.total_amount,
    },
  });

  await docClient.send(command);
  return await queryOrderById(order_id);
};

const updateUser = async (id, updates) => {
  var email_str = "#email", name_str = "#name", pass_str = "#password";
  var expression_values = {};
  if(updates.email){ expression_values[':email'] = updates.email; email_str = ":email"; }
  if(updates.name){ expression_values[':name'] = updates.name; name_str = ":name"; }
  if(updates.password){ expression_values[':password'] = updates.password; pass_str = ":password"; }
  const command = new UpdateCommand({
    TableName: "Users",
    Key: {
      id: id,
    },
    UpdateExpression: `set #email = ${email_str}, #name = ${name_str}, #password = ${pass_str}`,
    ExpressionAttributeNames: {
      "#email": "email",
      "#name" : "name",
      "#password" : "password",
    },
    ExpressionAttributeValues: expression_values,
  });

  await docClient.send(command);
  return await queryUserById(id);
};

module.exports = {
  init,
  queryRandomProduct,
  queryProductById,
  queryAllProducts,
  queryAllCategories,
  queryAllOrders,
  queryOrderById,
  queryOrdersByUser,
  queryUserById,
  queryAllUsers,
  insertOrder,
  updateUser,
};
